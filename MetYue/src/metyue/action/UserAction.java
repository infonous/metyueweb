package metyue.action;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import metyue.model.ActionResult;
import metyue.model.UserBaseInfo;
import metyue.model.UserBaseInfoVO;
import metyue.model.UserContactInfo;
import metyue.model.UserContactInfoVO;
import metyue.model.UserInfo;
import metyue.model.UserOnlineStatus;
import metyue.model.UserOptLogo;
import metyue.model.UserPhotoListInfo;
import metyue.model.UserPhotoListVO;
import metyue.model.UserPurposeInfo;
import metyue.model.UserPurposeInfoVO;
import metyue.model.UserRejectStatus;
import metyue.model.UserSessionInfo;
import metyue.service.ActionResultService;
import metyue.service.ImprintService;
import metyue.service.UserService;
import metyue.utils.EasyImage;
import metyue.utils.MD5Util;
import metyue.utils.WebUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.WebUtils;

@Controller
@RequestMapping(value = "/user")
public class UserAction {

	@Autowired
	private ActionResultService actionResultService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ImprintService imprintService;
	
	
	//change photo cover
	@RequestMapping(value = "/displayImage")
	public void displayImage(HttpServletRequest request, HttpServletResponse response){
		
		ActionResult objResult = null;
		
		String photoListPtr = WebUtils.findParameterValue(request, "PhotoListPtr");
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{				
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){	

				if("".equals(photoListPtr)){
					
					objResult = actionResultService.getActionResultItem("10001");
					
				}else{
					
					long userPtr = (long)session.getUserPtr();
					
					userService.displayPhotoList(userPtr, Long.parseLong(photoListPtr), WebUtil.USER_PHOTO_DEFAULT_NAME);
					
					//保存系统记录
					UserOptLogo userOptLogo = new UserOptLogo(
							userPtr, 
							appSource, 
							WebUtil.USER_CHANGE_PHOTO_COVER, 
							"changePhotoDisplay", 
							photoListPtr, 
							userPtr, 
							new Date()
						);
					
					imprintService.saveUserOptLogo(userOptLogo);
					
					objResult = actionResultService.getActionResultItem("20019");
					
				}

			}else{
				
				objResult = actionResultService.getActionResultItem("20008");
				
			}
			
		}catch (Exception e) {
			
			objResult = actionResultService.getActionResultItem("10000");
			
			e.printStackTrace();
			
		}
		
		WebUtil.responseToJson( response, objResult ); 
		
	}
	
	
	//change delete Image File
	@RequestMapping(value = "/deleteImageFile")
	public void deleteImageFile(HttpServletRequest request, HttpServletResponse response){
		
		ActionResult objResult = null;
		
		String photoListPtr = WebUtils.findParameterValue(request, "PhotoListPtr");
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{				
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){	

				if("".equals(photoListPtr)){
					
					objResult = actionResultService.getActionResultItem("10001");
					
				}else{
					
					long userPtr = (long)session.getUserPtr();
					
					UserPhotoListInfo userPhotoListInfo = userService.delPhotoList(Long.parseLong(photoListPtr));
					
					//保存系统记录
					UserOptLogo userOptLogo = new UserOptLogo(
							userPtr, 
							appSource, 
							WebUtil.USER_DEL_PHOTO_IMAGE, 
							"deleteImageFile", 
							photoListPtr, 
							userPtr, 
							new Date()
						);
					
					imprintService.saveUserOptLogo(userOptLogo);
					
					//物理删除文件
					if(userPhotoListInfo != null){
						
						try{

							File file = new File(userPhotoListInfo.getPhysicsPath(), userPhotoListInfo.getFileName());
							if(file.exists()){
								file.delete();
							}							
							
						}catch(Exception e) {
							
							objResult = actionResultService.getActionResultItem("20026");
							
							e.printStackTrace();
							
						}
					}
					
					objResult = actionResultService.getActionResultItem("20019");
					
				}

			}else{
				
				objResult = actionResultService.getActionResultItem("20008");
				
			}
			
		}catch (Exception e) {
			
			objResult = actionResultService.getActionResultItem("10000");
			
			e.printStackTrace();
			
		}
		
		WebUtil.responseToJson( response, objResult ); 
		
	}
	
	//change photo cover
	@RequestMapping(value = "/changePhotoCover")
	public void changePhotoCover(HttpServletRequest request, HttpServletResponse response){
		
		ActionResult objResult = null;
		
		String photoListPtr = WebUtils.findParameterValue(request, "PhotoListPtr");
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{				
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){	

				if("".equals(photoListPtr)){
					
					objResult = actionResultService.getActionResultItem("10001");
					
				}else{
					
					long userPtr = (long)session.getUserPtr();
					
					userService.changePhotoCover(userPtr, Long.parseLong(photoListPtr), WebUtil.USER_PHOTO_DEFAULT_NAME);
					
					//保存系统记录
					UserOptLogo userOptLogo = new UserOptLogo(
							userPtr, 
							appSource, 
							WebUtil.USER_CHANGE_PHOTO_COVER, 
							"changePhotoCover", 
							photoListPtr, 
							userPtr, 
							new Date()
						);
					
					imprintService.saveUserOptLogo(userOptLogo);
					
					objResult = actionResultService.getActionResultItem("20019");
					
				}

			}else{
				
				objResult = actionResultService.getActionResultItem("20008");
				
			}
			
		}catch (Exception e) {
			
			objResult = actionResultService.getActionResultItem("10000");
			
			e.printStackTrace();
			
		}
		
		WebUtil.responseToJson( response, objResult ); 
	}
	
	
	//change user pwd
	@RequestMapping(value = "/changeUserPwd")
	public void changeUserPwd(HttpServletRequest request, HttpServletResponse response){
		
		String pwd = WebUtils.findParameterValue(request, "Pwd");
		String pwd1 = WebUtils.findParameterValue(request, "Pwd1");
		String Pwd2 = WebUtils.findParameterValue(request, "Pwd2");
		
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		ActionResult objResult = null;
		
		if(!pwd1.equals(Pwd2)){
			
			objResult = actionResultService.getActionResultItem("20018");			
			
		}else{
		
			try{				
				
				UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
				
				if(session != null){				
					
					long userPtr = (long)session.getUserPtr();
					
					UserInfo userInfo = userService.getUserInfoByPkey(userPtr);
					
					String oldPwd = userInfo.getUserPwd();
					
					pwd = MD5Util.MD5(pwd);
					
					if(oldPwd.equals(pwd)){
						
						userInfo.setUserPwd( MD5Util.MD5(pwd1) );
						
						userService.update(userInfo);	//修改密码
						
						//保存系统记录
						UserOptLogo userOptLogo = new UserOptLogo(
								userPtr, 
								appSource, 
								WebUtil.USER_CHANGE_PWD, 
								"changeUserPwd", 
								"", 
								userPtr, 
								new Date()
							);
						
						imprintService.saveUserOptLogo(userOptLogo);
						
						
						objResult = actionResultService.getActionResultItem("20015");
						
					}else{
						
						objResult = actionResultService.getActionResultItem("20016");
					}
	
				}else{				
				
					objResult = actionResultService.getActionResultItem("20008");
				
				}
			
			}catch (Exception e) {			
	
				objResult = actionResultService.getActionResultItem("10000");
				
				e.printStackTrace();
			
			}
		}
		
		WebUtil.responseToJson( response, objResult ); 
		
	}
	
	//forget user PWD: check user Answer
	@RequestMapping(value = "/checkUserAnswer")
	public void checkUserAnswer(HttpServletRequest request, HttpServletResponse response){
		
		String userEmail = WebUtils.findParameterValue(request, "Email");		
		String userAnswer = WebUtil.decode2(WebUtils.findParameterValue(request, "Answer"));		
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		ActionResult objResult = null;
		
		if (!("".equals(userEmail) && "".equals(userAnswer))){
			
			try{
				
				UserInfo userInfo = userService.getUserInfoByEmail(userEmail);
				
				String realAnswer = userInfo.getUserAnswer().toString();
				
				if(realAnswer.equals(userAnswer)){					
					
					String newPwd = WebUtil.makeUserNewPwd();
					
					userInfo.setUserPwd( MD5Util.MD5(newPwd) );
					
					userService.update(userInfo);	//修改密码
					
					long userPtr = userInfo.getPkey();
					
					//保存系统记录
					UserOptLogo userOptLogo = new UserOptLogo(
							userPtr, 
							appSource, 
							WebUtil.USER_FORGET_PWD, 
							"checkUserAnswer", 
							"", 
							userPtr, 
							new Date()
						);
					
					imprintService.saveUserOptLogo(userOptLogo);
					
					objResult= actionResultService.getActionResultItem("10005", newPwd );
					
				}else{
					
					objResult= actionResultService.getActionResultItem("10004", "");
				}
				
			}catch (Exception e) {	
				
				objResult = actionResultService.getActionResultItem("10000");
				
				e.printStackTrace();
				
			}
		}
		
		WebUtil.responseToJson( response, objResult ); 
		
	}	

	
	//forget user PWD: get Question
	@RequestMapping(value = "/getUserQuestion")
	public void getUserQuestion(HttpServletRequest request, HttpServletResponse response){
		
		String userEmail = WebUtils.findParameterValue(request, "Email");
		
		ActionResult objResult = null;
		
		if(!"".equals(userEmail)){
			
			try{
				
				UserInfo userInfo = userService.getUserInfoByEmail(userEmail);
				
				if(userInfo != null){
					
					objResult= actionResultService.getActionResultItem("10003", userInfo.getUserQuestion().toString());
					
				}else{				
					
					objResult= actionResultService.getActionResultItem("10006", "");
					
				}
				
			}catch (Exception e) {	
				
				objResult = actionResultService.getActionResultItem("10000");
				
				e.printStackTrace();
				
			}
			
		}else{
			
			objResult = actionResultService.getActionResultItem("10001");
			
		}
		
		WebUtil.responseToJson( response, objResult ); 
		
	}
	
	//save Photo list info
	@RequestMapping(value = "/UploadUserPhoto")
	public void uploadUserPhoto(@RequestParam("AppSource") String AppSource, 
			@RequestParam("UserImagesFile") MultipartFile UserImagesFile, 
			HttpServletRequest request, HttpServletResponse response ) throws Exception{
		
		ActionResult objResult = null;
		
		try{				
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){	
				
				long userPtr = session.getUserPtr();
				
				String fileName = "";
				String mkImagesDir = WebUtil.USER_PHOTO_PYSI_FILE_PATH + userPtr + "/";
				long height = 0L;
				long width = 0L;
				
				if(!UserImagesFile.isEmpty()){
					
					//生成新的文件名称：包括缩微图片
					fileName = UserImagesFile.getOriginalFilename();					
					SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
					fileName = df.format(new Date()) + fileName.substring(fileName.lastIndexOf("."));
					
					//图片完整路径
					String path = mkImagesDir + fileName;
					String pathResize = mkImagesDir + "r_" + fileName;
					
					//保存图片
					UserImagesFile.transferTo(new File(path));	
					
					//生成缩微图片
					EasyImage img = new EasyImage(path);
					width = img.getWidth();
					height = img.getHeight();
					img.resize( (width > 160 ? 160 : (int)width), -1); //已经缩放，重新获取长宽
					width = img.getWidth();
					height = img.getHeight();
					//img.crop(0, 0, (width > 160 ? 160 : -1), (height > 159 ? 159 : -1));
					img.resize( -1, (height > 160 ? 160 : (int)height));
					img.saveAs(pathResize);
				}
				
				UserPhotoListInfo item = new UserPhotoListInfo(
						WebUtil.USER_PHOTO_PYSI_FILE_PATH, 
						fileName,
						userPtr, 
							new Date(), 
							"",
							WebUtil.USER_PHOTO_URL, 
							WebUtil.USER_PHOTO_INIT_STATUS,
							"0",
							width,
							height,
							"1"
						);
				
				userService.saveUserPhotoListInfo(item, userPtr, WebUtil.USER_PHOTO_DEFAULT_NAME);
				
				//保存记录
				UserOptLogo userOptLogo = new UserOptLogo(
						userPtr, 
						AppSource, 
						WebUtil.SAVE_USER_PHOTO_LIST_INFO, 
						"UploadUserPhoto", 
						"", 
						userPtr, 
						new Date()
					);
				
				imprintService.saveUserOptLogo(userOptLogo);				
				
				objResult = actionResultService.getActionResultItem("20013");
				
			}else{				
				
				objResult = actionResultService.getActionResultItem("20008");
				
			}
			
		}catch (Exception e) {	
			

			objResult = actionResultService.getActionResultItem("10000");
			
			e.printStackTrace();
			
		}
		
		WebUtil.responseToJson( response, objResult ); 
	}
	

	//get Purpose Info vo
	@RequestMapping(value = "/getUserPhotoListInfo")
	public void getUserPhotoListInfo(HttpServletRequest request, HttpServletResponse response){
		
		long pkey = -1L;
		long sessionUserPtr = -1L;
		boolean isMyself = false;
		
		String userPtr = WebUtils.findParameterValue(request, "Pkey");
		
		UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
		
		if(session != null){
			
			sessionUserPtr = session.getUserPtr();
			
		}
		
		if( ("".equals(userPtr)) || (userPtr == null) ) {
			
			pkey = sessionUserPtr;
			
			isMyself = true;
			
		}else{
			
			pkey = Long.valueOf( userPtr );
			
			if(pkey==sessionUserPtr){
				
				isMyself = true;
			}
		}
		
		
		//获取相应的用户基本资料的PKEY
		if(pkey > 0L){
			
			List<UserPhotoListVO> list = userService.getUserPhotoInfoVOList(pkey, WebUtil.USER_PHOTO_DEFAULT_NAME, isMyself);
			
			WebUtil.responseToJson( response, list ); 
			
		}else{
			
			WebUtil.responseToJson( response, actionResultService.getActionResultItem("10001") ); 
		}
	}
	
	//get Purpose Info vo
	@RequestMapping(value = "/getUserPurposeInfo")
	public void getUserPurposeInfo(HttpServletRequest request, HttpServletResponse response){
		
		long pkey = -1L;
		
		String userPtr = WebUtils.findParameterValue(request, "Pkey");	
		
		if( ("".equals(userPtr)) || (userPtr == null) ) {
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				pkey = session.getUserPtr();
			}
			
		}else{
			
			pkey = Long.valueOf( userPtr );
		}
		
		
		//获取相应的用户基本资料的PKEY
		if(pkey > 0L){
			
			UserPurposeInfo userPurposeInfo = userService.getUserPurposeInfoByPtr(pkey);
			
			UserPurposeInfoVO UserPurposeInfoVO = new UserPurposeInfoVO(
						userPurposeInfo.getUserPtr(), 
						userPurposeInfo.getPlan(), 
						userPurposeInfo.getExpress(),
						userPurposeInfo.getManner(), 
						userPurposeInfo.getPurposeDesc()
					);				
			
			WebUtil.responseToJson( response, UserPurposeInfoVO ); 
			
		}else{
			
			WebUtil.responseToJson( response, actionResultService.getActionResultItem("10001") ); 
		}
		
	}
	
	//save purpose info
	@RequestMapping(value = "/saveUserPurposeInfo")
	public void saveUserPurposeInfo(HttpServletRequest request, HttpServletResponse response){
		
		String plan = WebUtils.findParameterValue(request, "Plan");
		String express = WebUtils.findParameterValue(request, "Express");
		String manner = WebUtils.findParameterValue(request, "Manner");
		String desc = WebUtil.decode2( WebUtils.findParameterValue(request, "Descript") );
		
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){	
				
				long userPtr = session.getUserPtr();
				
				UserPurposeInfo userPurposeInfo = new UserPurposeInfo( 
						userPtr, 
						plan, 
						express,
						manner, 
						desc, 
						new Date(), 
						userPtr);
				
				userService.saveUserPurposeInfo(userPurposeInfo);
				
				//保存记录
				UserOptLogo userOptLogo = new UserOptLogo(
						userPtr, 
						appSource, 
						WebUtil.SAVE_USER_PURPOSE_INFO, 
						"saveUserPurposeInfo", 
						"", 
						userPtr, 
						new Date()
					);
				
				imprintService.saveUserOptLogo(userOptLogo);
				
				//Save success
				WebUtil.responseToJson( response, actionResultService.getActionResultItem("20011") ); 
				
			}else{
				
				WebUtil.responseToJson( response, actionResultService.getActionResultItem("20008") ); 
			}
			
			
		}catch (Exception e) {	
			
			WebUtil.responseToJson( response, actionResultService.getActionResultItem("10000") ); 		
			e.printStackTrace();
			
		}
	}
	
	
	//get Contact Info vo
	@RequestMapping(value = "/getUserContactInfo")
	public void getUserContactInfo(HttpServletRequest request, HttpServletResponse response){
		
		long pkey = -1L;
		
		String userPtr = WebUtils.findParameterValue(request, "Pkey");	
		
		if( ("".equals(userPtr)) || (userPtr == null) ) {
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				pkey = session.getUserPtr();
			}
			
		}else{
			
			pkey = Long.valueOf( userPtr );
		}
		
		
		//获取相应的用户基本资料的PKEY
		if(pkey > 0L){
			
			UserContactInfo userContactInfo = userService.getUserContactInfoByPtr(pkey);
			
			UserContactInfoVO userContactInfoVO = new UserContactInfoVO(
						userContactInfo.getUserPtr(), 
						userContactInfo.getTencentNum(),
						userContactInfo.getTencentWeixien(), 
						userContactInfo.getSinaWeibo(), 
						userContactInfo.getRenrenUrl(),
						userContactInfo.getTel(),
						userContactInfo.getOthContactBy()
					);				
			
			WebUtil.responseToJson( response, userContactInfoVO ); 
			
		}else{
			
			WebUtil.responseToJson( response, actionResultService.getActionResultItem("10001") ); 
		}
		
	}
	
	//Save contact info
	@RequestMapping(value = "/saveUserContactInfo")
	public void saveUserContactInfo(HttpServletRequest request, HttpServletResponse response){
		
		String qq = WebUtils.findParameterValue(request, "QQ");
		String weiXien = WebUtils.findParameterValue(request, "WeiXien");
		String weiBo = WebUtils.findParameterValue(request, "WeiBo");
		String renRen = WebUtils.findParameterValue(request, "RenRen");
		String tel = WebUtils.findParameterValue(request, "Tel");
		String other = WebUtils.findParameterValue(request, "Other");
		
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){	
				
				long userPtr = session.getUserPtr();
				
				UserContactInfo userContactInfo = new UserContactInfo( 
						userPtr, 
						qq,
						weiXien, 
						weiBo, 
						renRen,
						tel, 
						other, 
						new Date(), 
						userPtr);
				
				userService.saveUserContactInfo(userContactInfo);
				
				//保存退出系统记录
				UserOptLogo userOptLogo = new UserOptLogo(
						userPtr, 
						appSource, 
						WebUtil.SAVE_USER_CONTACT_INFO, 
						"saveUserContactInfo", 
						"", 
						userPtr, 
						new Date()
					);
				
				imprintService.saveUserOptLogo(userOptLogo);
				
				//Save success
				WebUtil.responseToJson( response, actionResultService.getActionResultItem("20011") ); 
				
			}else{
				
				WebUtil.responseToJson( response, actionResultService.getActionResultItem("20008") ); 
			}
			
			
		}catch (Exception e) {	
			
			WebUtil.responseToJson( response, actionResultService.getActionResultItem("10000") ); 		
			e.printStackTrace();
			
		}
	}
	
	//get UserBase Info vo
	@RequestMapping(value = "/getUserBaseInfo")
	public void getUserBaseInfo(HttpServletRequest request, HttpServletResponse response){
		
		long pkey = -1L;
		
		String userPtr = WebUtils.findParameterValue(request, "Pkey");	
		
		if( ("".equals(userPtr)) || (userPtr == null) ) {
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				pkey = session.getUserPtr();
			}
			
		}else{
			
			pkey = Long.valueOf( userPtr );
		}
		
		
		//获取相应的用户基本资料的PKEY
		if(pkey > 0L){
			
			UserBaseInfo userBaseInfo = userService.getUserBaseInfoByPtr(pkey);
			
			UserBaseInfoVO userBaseInfoVO = new UserBaseInfoVO(
						userBaseInfo.getUserPtr(), 
						userBaseInfo.getAge(), 
						userBaseInfo.getCurrentAddress(),
						userBaseInfo.getHeight(), 
						userBaseInfo.getWeight(), 
						userBaseInfo.getMarry(), 
						userBaseInfo.getJob(),
						userBaseInfo.getEducation()
					);
			
			WebUtil.responseToJson( response, userBaseInfoVO ); 
			
		}else{
			
			WebUtil.responseToJson( response, actionResultService.getActionResultItem("10001") ); 
		}
		
	}
	
	//Save UserBase Info
	@RequestMapping(value = "/saveUserBaseInfo")
	public void saveUserBaseInfo(HttpServletRequest request, HttpServletResponse response){
		
		String age = WebUtils.findParameterValue(request, "Age");
		String currentAddress = WebUtil.decode2( WebUtils.findParameterValue(request, "Address") );
		String height = WebUtils.findParameterValue(request, "Height");
		String weight = WebUtils.findParameterValue(request, "Weight");
		String marry = WebUtils.findParameterValue(request, "Marry");
		String job = WebUtils.findParameterValue(request, "Job");
		String education = WebUtils.findParameterValue(request, "Education");
		
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){	
				
				long userPtr = session.getUserPtr();
				
				UserBaseInfo userBaseInfo = new UserBaseInfo( 
						userPtr,
						Integer.valueOf(age), 
						currentAddress, 
						Integer.valueOf(height), 
						Integer.valueOf(weight), 
						marry, 
						job, 
						education, 
						new Date(), userPtr);
				
				userService.saveUserBaseInfo(userBaseInfo);
				
				//保存记录
				UserOptLogo userOptLogo = new UserOptLogo(
						userPtr, 
						appSource, 
						WebUtil.SAVE_USER_BASE_INFO, 
						"saveUserBaseInfo", 
						"", 
						userPtr, 
						new Date()
					);
				
				imprintService.saveUserOptLogo(userOptLogo);
				
				//Save success
				WebUtil.responseToJson( response, actionResultService.getActionResultItem("20011") ); 
				
			}else{
				
				WebUtil.responseToJson( response, actionResultService.getActionResultItem("20008") ); 
			}
			
			
		}catch (Exception e) {	
			
			WebUtil.responseToJson( response, actionResultService.getActionResultItem("10000") ); 		
			e.printStackTrace();
			
		}
	}
	
	//用户登录 Email 是否已存在
	@RequestMapping(value = "/checkExistByEmail")
	public void checkExistByEmail(HttpServletRequest request, HttpServletResponse response){
		
		String email = WebUtils.findParameterValue(request, "Email");
		
		try{
			
			boolean isExistByEmail = userService.checkUserExistByEmail(email);
			
			WebUtil.responseToJson( response, actionResultService.getActionResultItem( (isExistByEmail ? "20001" : "20002") ) );
			
		}catch (Exception e) {	
			
			WebUtil.responseToJson( response, actionResultService.getActionResultItem("10000") ); 		
			e.printStackTrace();
			
		}
	}
	
	//用户退出系统
	@RequestMapping(value = "/logout")
	public void logout(HttpServletRequest request, HttpServletResponse response){
		
		String userEmail = WebUtil.decode2( WebUtils.findParameterValue(request, "Email") );
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){				
				
				long userPtr = session.getUserPtr();				
				
				//自动保存日志、状态和权限记录				
				UserOnlineStatus userOnlineStatus = new UserOnlineStatus(
						userPtr, 
						new Date(), 
						new Date(),
						appSource, 
						userPtr, 
						new Date()
					);				
				
				//保存退出系统记录
				UserOptLogo userOptLogo = new UserOptLogo(
						userPtr, 
						appSource, 
						WebUtil.OPT_LOGO_LOGOUT, 
						"logout", 
						"", 
						userPtr, 
						new Date()
					);
				
				imprintService.saveUserImprint(userOnlineStatus, userOptLogo);
				
				//清空连接 Session
				WebUtil.saveSession(request, "UserInfo", null);
				
				//输出结果
				WebUtil.responseToJson( response, actionResultService.getActionResultItem("20009") );
				
			}else{				
				
				if(userEmail != null){
					
					//如果用户的 Session 已经中止，记录为 AUTO_OFF_LINE
					long userPtr = userService.getUserInfoPtrByEmail(userEmail);
					
					//保存退出系统记录: 存在匿名用户故意的操作的逻辑漏洞，不过 AUTO_OFF_LINE 不作LOGOUT 的判断依据
					UserOptLogo userOptLogo = new UserOptLogo(
							userPtr, 
							appSource, 
							WebUtil.OPT_LOGO_AUTO_OFF_LINE, 
							"logout", 
							"", 
							userPtr, 
							new Date()
						);
					
					imprintService.saveUserOptLogo(userOptLogo);
					
					WebUtil.responseToJson( response, actionResultService.getActionResultItem("20010") ); 
					
				}else{
					
					WebUtil.responseToJson( response, actionResultService.getActionResultItem("10001") ); 
				}
			}
			
		}catch (Exception e) {	
			
			WebUtil.responseToJson( response, actionResultService.getActionResultItem("10000") ); 		
			e.printStackTrace();
			
		}
		
	}
	
	//用户是否已经登录
	@RequestMapping(value = "/checkOnline")
	public void checkOnline(HttpServletRequest request, HttpServletResponse response){
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				WebUtil.responseToJson( response, actionResultService.getActionResultItem("20007") ); 
				
			}else{
				
				WebUtil.responseToJson( response, actionResultService.getActionResultItem("20008") ); 
			}
			
		}catch (Exception e) {
			
			WebUtil.responseToJson( response, actionResultService.getActionResultItem("10000") ); 
			
			e.printStackTrace();
		}
	}
	
	
	//用户登录
	@RequestMapping(value = "/login")
	public void login(HttpServletRequest request, HttpServletResponse response) {
		
		String userEmail = WebUtil.decode2( WebUtils.findParameterValue(request, "Email") );
		String userPwd = WebUtils.findParameterValue(request, "Pwd");
		
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{
			
			UserInfo userInfo = userService.login(userEmail, userPwd);
			
			if((userInfo != null)&&(userInfo.getPkey() != null)){
				
				long userPtr = userInfo.getPkey();	
				
				//保存 user session
				UserSessionInfo session = imprintService.getUserSessionInfo(userPtr);
				WebUtil.saveSession(request, "UserInfo", session);
				
				//自动保存日志、状态和权限记录				
				UserOnlineStatus userOnlineStatus = new UserOnlineStatus(
						userPtr, 
						new Date(), 
						new Date(),
						appSource, 
						userPtr, 
						new Date()
					);
				
				UserOptLogo userOptLogo = new UserOptLogo(
							userPtr, 
							appSource, 
							WebUtil.OPT_LOGO_LOGIN, 
							"login", 
							"", 
							userPtr, 
							new Date()
						);
				
				imprintService.saveUserImprint(userOnlineStatus, userOptLogo);
				
				//输出结果
				WebUtil.responseToJson( response, actionResultService.getActionResultItem("20005") ); 
				
			}else{
				
				WebUtil.responseToJson( response, actionResultService.getActionResultItem("20006") ); 
			}
			
		}catch (Exception e) {
			
			WebUtil.responseToJson( response, actionResultService.getActionResultItem("10000") ); 
			
			e.printStackTrace();
		}
		
	}
	
	
	//注册用户帐号
	@RequestMapping(value = "/register")
	public void register(HttpServletRequest request, HttpServletResponse response) {
		
		String userName = WebUtil.decode2( WebUtils.findParameterValue(request, "Name") );
		String userEmail = WebUtil.decode2( WebUtils.findParameterValue(request, "Email") );
		String userPwd = MD5Util.MD5( WebUtil.decode2( WebUtils.findParameterValue(request, "Pwd")) );
		String sex = WebUtil.decode2( WebUtils.findParameterValue(request, "Sex") );
		String userQuestion = WebUtil.decode2( WebUtils.findParameterValue(request, "Question") ) ;
		String userAnswer = WebUtil.decode2( WebUtils.findParameterValue(request, "Answer") );
		
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{
			
			boolean isExistByEmail = userService.checkUserExistByEmail(userEmail);
		     
			if(!isExistByEmail){
				
				//保存主体资料
				UserInfo userInfo = new UserInfo(
						userName, 
						userEmail, 
						userPwd,
						sex, 
						userQuestion, 
						userAnswer,
						new Date()
				);
				
				userInfo = userService.register(userInfo);
				
				
				if((userInfo != null)&&(userInfo.getPkey() != null)){
					
					long userPtr = userInfo.getPkey();	
					
					//保存 user session
					UserSessionInfo session = imprintService.getUserSessionInfo(userPtr);
					WebUtil.saveSession(request, "UserInfo", session);
				
					//自动保存日志、状态和权限记录				
					UserOnlineStatus userOnlineStatus = new UserOnlineStatus(
							userPtr, 
							new Date(), 
							new Date(),
							appSource, 
							userPtr, 
							new Date()
						);
					
					UserOptLogo userOptLogo = new UserOptLogo(
								userPtr, 
								appSource, 
								WebUtil.OPT_LOGO_REG, 
								"register", 
								(userName + ";" + userEmail ), 
								userPtr, 
								new Date()
							);
					
					UserRejectStatus userRejectStatus = new UserRejectStatus(userPtr, userPtr, new Date());
					
					imprintService.saveNewUserImprint(userOnlineStatus, userOptLogo, userRejectStatus);
					
					//自动创建用户的图片保存目录
					String mkImagesDir = WebUtil.USER_PHOTO_PYSI_FILE_PATH + userPtr + "/";
					File dir = new File(mkImagesDir);
					dir.mkdir();			
					
					//输出结果
					WebUtil.responseToJson( response, actionResultService.getActionResultItem( "20003") );
				
				}else{
					
					WebUtil.responseToJson( response, actionResultService.getActionResultItem( "20004") );
				}
				
				
			}else{
				
				WebUtil.responseToJson( response, actionResultService.getActionResultItem( "20002") );
			}
			
		}catch (Exception e) {
			
			WebUtil.responseToJson( response, actionResultService.getActionResultItem("10000") ); 
			e.printStackTrace();
		}
		
	}
	
	
}
