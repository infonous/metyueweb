package metyue.action;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import metyue.model.ActionResult;
import metyue.model.FilterContent;
import metyue.model.FilterUserImage;
import metyue.model.UserOptLogo;
import metyue.model.UserPhotoListInfo;
import metyue.model.UserSessionInfo;
import metyue.model.UserTalkInfo;
import metyue.service.ActionResultService;
import metyue.service.ContentManageService;
import metyue.service.ImprintService;
import metyue.service.UserTalkService;
import metyue.utils.WebUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.WebUtils;

@Controller
@RequestMapping(value = "/manage")
public class ContentManageAction {

	@Autowired
	private UserTalkService userTalkService;
	
	@Autowired
	private ActionResultService actionResultService;
	
	@Autowired
	private ContentManageService contentManageService;
	
	@Autowired
	private ImprintService imprintService;
	
	//查询可能带有敏感词的用户：姓名 + 交友宣言
	@RequestMapping(value = "/getFilterLists")
	public void getFilterLists(HttpServletRequest request, HttpServletResponse response){
		
		ActionResult objResult = null;
		
		String keyWords = WebUtil.decode2( WebUtils.findParameterValue(request, "KeyWords") );
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				String manageLevel = session.getManageLevel();
				
				if("1".equals(manageLevel)){
					
					List<FilterContent> lst = contentManageService.getFilterLists(keyWords);
					
					WebUtil.responseToJson( response, lst );
					
				}else{
					
					objResult = actionResultService.getActionResultItem("30002");
					
					WebUtil.responseToJson( response, objResult );
				}
				
			}else{
				
				objResult = actionResultService.getActionResultItem("20008");
				
				WebUtil.responseToJson( response, objResult );
				
			}
			
		}catch (Exception e) {
			
			e.printStackTrace();
			
			objResult = actionResultService.getActionResultItem("10000");
			
			WebUtil.responseToJson( response, objResult );
			
		}		
		 
	}
	
	@RequestMapping(value = "/getFilterImageLists")
	public void getFilterImageLists(HttpServletRequest request, HttpServletResponse response){
		
		ActionResult objResult = null;
		
		String userName = WebUtil.decode2( WebUtils.findParameterValue(request, "UserName") );
		
		String imageFileName = WebUtils.findParameterValue(request, "ImageFileName");
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				String manageLevel = session.getManageLevel();
				
				if("1".equals(manageLevel)){
					
					List<FilterUserImage> lst = contentManageService.getFilterImageLists(userName, imageFileName);
					
					WebUtil.responseToJson( response, lst );
					
				}else{
					
					objResult = actionResultService.getActionResultItem("30002");
					
					WebUtil.responseToJson( response, objResult );
				}
				
			}else{
				
				objResult = actionResultService.getActionResultItem("20008");
				
				WebUtil.responseToJson( response, objResult );
				
			}
			
		}catch (Exception e) {
			
			e.printStackTrace();
			
			objResult = actionResultService.getActionResultItem("10000");
			
			WebUtil.responseToJson( response, objResult );
			
		}
		
	}
	
	@RequestMapping(value = "/filterUserName")
	public void filterUserName(HttpServletRequest request, HttpServletResponse response){
		
		ActionResult objResult = null;
		
		String userPtr = WebUtils.findParameterValue(request, "UserPtr");
		
		String userName = WebUtil.decode2(  WebUtils.findParameterValue(request, "UserName") );
		
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				String manageLevel = session.getManageLevel();
				
				long optPtr = (long)session.getUserPtr();
				
				if("1".equals(manageLevel)){
					
					contentManageService.filterUserName(Long.parseLong(userPtr), userName);
					
					//保存系统记录
					UserOptLogo userOptLogo = new UserOptLogo(
							optPtr, 
							appSource, 
							WebUtil.MANAGE_CHANGE_USER_NAME, 							
							"ContentManageAction",
							userPtr, 				
							optPtr, 
							new Date()
						);
					
					imprintService.saveUserOptLogo(userOptLogo);
					
					objResult = actionResultService.getActionResultItem("30003");

					
				}else{
					
					objResult = actionResultService.getActionResultItem("30002");
				}
				
			}else{
				
				objResult = actionResultService.getActionResultItem("20008");
				
			}
			
		}catch (Exception e) {
			
			e.printStackTrace();
			
			objResult = actionResultService.getActionResultItem("10000");
			
		}
		
		WebUtil.responseToJson( response, objResult );
		
	}
	
	@RequestMapping(value = "/filterPurpose")
	public void filterPurpose(HttpServletRequest request, HttpServletResponse response){
		
		ActionResult objResult = null;
		
		String userPtr = WebUtils.findParameterValue(request, "UserPtr");
		
		String purposeDesc = WebUtil.decode2( WebUtils.findParameterValue(request, "PurposeDesc") );
		
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				String manageLevel = session.getManageLevel();
				
				long optPtr = (long)session.getUserPtr();
				
				if("1".equals(manageLevel)){
					
					contentManageService.filterPurpose(Long.parseLong(userPtr), purposeDesc);
					
					//保存系统记录
					UserOptLogo userOptLogo = new UserOptLogo(
							optPtr, 
							appSource, 
							WebUtil.MANAGE_CHANGE_USER_PURPOSE, 							
							"ContentManageAction",
							userPtr, 				
							optPtr, 
							new Date()
						);
					
					imprintService.saveUserOptLogo(userOptLogo);
					
					objResult = actionResultService.getActionResultItem("30003");

					
				}else{
					
					objResult = actionResultService.getActionResultItem("30002");
				}
				
			}else{
				
				objResult = actionResultService.getActionResultItem("20008");
				
			}
			
		}catch (Exception e) {
			
			e.printStackTrace();
			
			objResult = actionResultService.getActionResultItem("10000");
			
		}
		
		WebUtil.responseToJson( response, objResult );
		
	}
	
	@RequestMapping(value = "/checkUploadImage")
	public void checkUploadImage(HttpServletRequest request, HttpServletResponse response){

		ActionResult objResult = null;
		
		String userPtr = WebUtils.findParameterValue(request, "UserPtr");
		
		String photoListPtr = WebUtil.decode2( WebUtils.findParameterValue(request, "PhotoListPtr") );
		
		String appSource = WebUtil.decode2( WebUtils.findParameterValue(request, "AppSource") );
		
		try{
			
			UserSessionInfo session = (UserSessionInfo)WebUtil.getUserSession(request, "UserInfo");
			
			if(session != null){
				
				String manageLevel = session.getManageLevel();
				
				long optPtr = (long)session.getUserPtr();
				
				if("1".equals(manageLevel)){
					
					contentManageService.checkUploadImage(Long.parseLong(userPtr), Long.parseLong(photoListPtr));
					
					//保存系统记录
					UserOptLogo userOptLogo = new UserOptLogo(
							optPtr, 
							appSource, 
							WebUtil.MANAGE_CHANGE_USER_IMAGE_CHECK, 							
							"ContentManageAction",
							photoListPtr, 				
							optPtr, 
							new Date()
						);
					
					imprintService.saveUserOptLogo(userOptLogo);					
					
					//按状态对图片文件作物理操作
					UserPhotoListInfo userPhotoListInfo = contentManageService.getImageCheckStatus(Long.parseLong(photoListPtr));
					String checkStatus = userPhotoListInfo.getCheckStatus();
					if("0".equals(checkStatus)){

						try{							

							//删除原图
							File o_file = new File(userPhotoListInfo.getPhysicsPath() + userPtr, userPhotoListInfo.getFileName());
							if(o_file.exists())
							{
								o_file.delete();
							}
							
							//删除缩微图
							File r_file = new File(userPhotoListInfo.getPhysicsPath() + userPtr, "r_" + userPhotoListInfo.getFileName());
							if(r_file.exists())
							{
								r_file.delete();
							}
							
						}catch (Exception e) {
							
							e.printStackTrace();
							
							objResult = actionResultService.getActionResultItem("20026");
							
						}
						
					}
					
					//操作者与管理者是相同的话，则不发送删除信息
					//发送删除信息: 构建信息实体
					if(optPtr != Long.parseLong(userPtr) ){
						
						UserTalkInfo userTalkInfo = new UserTalkInfo();
						userTalkInfo.setSendUserPtr(optPtr);
						userTalkInfo.setReceiveUserPtr(Long.parseLong(userPtr));
						userTalkInfo.setTalkAbout(WebUtil.MANAGE_CHANGE_USER_IMAGE_MESSAGE);
						userTalkInfo.setLastUserPtr(optPtr);
						userTalkInfo.setReadFlg("1");
						userTalkInfo.setLastUpdate(new Date());
						userTalkInfo.setAppSource(appSource);					
						userTalkService.send(userTalkInfo);	
					}					
					
					objResult = actionResultService.getActionResultItem("30003");

					
				}else{
					
					objResult = actionResultService.getActionResultItem("30002");
				}
				
			}else{
				
				objResult = actionResultService.getActionResultItem("20008");
				
			}
			
		}catch (Exception e) {
			
			e.printStackTrace();
			
			objResult = actionResultService.getActionResultItem("10000");
			
		}
		
		WebUtil.responseToJson( response, objResult );
		
	}

}
