package metyue.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import metyue.model.FilterContent;
import metyue.model.FilterUserImage;
import metyue.model.UserInfo;
import metyue.model.UserPhotoListInfo;
import metyue.model.UserPurposeInfo;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ContentManageDao {

	@Autowired
    private HibernateTemplate hibernateTemplate;
	
	//敏感：修改名称
	public void filterUserName(long userPtr, String userName){
		
		UserInfo userInfo = hibernateTemplate.get(UserInfo.class, userPtr);
		
		userInfo.setUserName(userName);
		
		userInfo.setLastUpdate(new Date());
		
		hibernateTemplate.update(userInfo);
		
	}
	
	//敏感：交友宣言
	public void filterPurpose(long userPtr, String purposeDesc){
		
		UserPurposeInfo userPurposeInfo = hibernateTemplate.get( UserPurposeInfo.class,  userPtr);
		
		UserInfo userInfo = hibernateTemplate.get(UserInfo.class, userPtr);		
	
		userPurposeInfo.setPurposeDesc(purposeDesc);
		
		userPurposeInfo.setUserInfo(userInfo);
		
		hibernateTemplate.update(userPurposeInfo);
		
	}
	
	//敏感：图片禁止或取消禁止
	//0 禁止; 1 不禁止; 弹性开关
	public void checkUploadImage(long userPtr, long photoListPtr){
		
		try{
			
			final StringBuilder queryStr = new StringBuilder();
			
			queryStr.append(" UPDATE user_photo_list_info  ");
			queryStr.append(" SET CHECK_STATUS=(CASE WHEN CHECK_STATUS='1' THEN '0'  ");
			//queryStr.append("    WHEN CHECK_STATUS='0' THEN '1' ");
			queryStr.append("    WHEN CHECK_STATUS IS NULL THEN '0' ");
			queryStr.append("    ELSE CHECK_STATUS END) ");				//功能仅限于修改 0、1 所代表的是否禁止标识
			queryStr.append(" WHERE PKEY='" + photoListPtr + "'" );			
			
			this.hibernateTemplate.execute(new HibernateCallback<Object>() {
		    	
		    	public Object doInHibernate(final Session session)throws HibernateException, SQLException {
		    		
		    		Query query = session.createSQLQuery(queryStr.toString());
		    		
		    		return query.executeUpdate();
		    	}
		    });
    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
	}
	
	//简单获取图片状态
	public UserPhotoListInfo getImageCheckStatus(long photoListPtr){
		
		UserPhotoListInfo userPhotoListInfo = hibernateTemplate.get(UserPhotoListInfo.class, photoListPtr);
		
		return userPhotoListInfo;
	}
	
	//细分关键词查询
	public String splitEqualSql(String columnsName, String splits){
		
		String result = "";
		
		if(splits.length() > 0){
			
			String[] values = splits.split(" ");
			int size = values.length;
			
			for(int n=0; n < size; n++){
			
				result += columnsName + " = '" + values[n] + "' " + ( (n+1) == size? "" : " or ");
			}
			
			result = "(" + result + ")";
			
		}
		
		return result;
	}
	
	//细分关键词查询
	public String splitLikeSql(String columnsName, String splits){
		
		String result = "";
		
		if(splits.length() > 0){
			
			String[] values = splits.split(" ");
			int size = values.length;
			
			for(int n=0; n < size; n++){
			
				result += columnsName + " like '%" + values[n] + "%' " + ( (n+1) == size? "" : " or ");
			}
			
			result = "(" + result + ")";
			
		}
		
		return result;
	}
	
	//敏感词查询
	@SuppressWarnings("unchecked")
	public List<FilterContent> getFilterLists(String keyWords){
		
		List<FilterContent> list = new ArrayList<FilterContent>();
		
		try {
			
			final StringBuilder queryStr = new StringBuilder();
						
			queryStr.append(" SELECT ");
			queryStr.append(" 	DISTINCT USER.PKEY, USER.USER_NAME,IFNULL(PURPOSE.PURPOSE_DESC,'〈此人很懒，交友宣言为空〉') AS PURPOSE_DESC ");
			queryStr.append(" FROM USER_INFO USER ");
			queryStr.append(" LEFT JOIN USER_PURPOSE_INFO PURPOSE ON USER.PKEY=PURPOSE.USER_PTR ");
			
			if(keyWords.length() > 0){
				queryStr.append(" WHERE ");			
				queryStr.append( " " + splitEqualSql("USER.USER_NAME", keyWords) );
				queryStr.append( " OR " + splitLikeSql("PURPOSE.PURPOSE_DESC", keyWords) );	
			}
			
			queryStr.append(" limit 20 ");	

			
		    list = (List<FilterContent>) this.hibernateTemplate.execute(new HibernateCallback<List<FilterContent>>() {
		    	
		    	public List<FilterContent> doInHibernate(final Session session)throws HibernateException, SQLException {
		    		
		    		Query query = session.createSQLQuery(queryStr.toString()).addEntity(FilterContent.class);
		    		
		    		return query.list();
		    	}
		    });

		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return list;		
	}
	
	
	//敏感词查询
	@SuppressWarnings("unchecked")
	public List<FilterUserImage> getFilterImageLists(String userName, String imageFileName){
		
		List<FilterUserImage> list = new ArrayList<FilterUserImage>();
		
		try {
			
			final StringBuilder queryStr = new StringBuilder();
						
			queryStr.append(" SELECT ");
			queryStr.append(" 	DISTINCT IMAGES.PKEY,USER.PKEY AS USER_PTR, ");
			queryStr.append(" 	(CONCAT('resources',IMAGES.URL_PATH,PHOTO.USER_PTR,'/',IMAGES.FILE_NAME)) AS IMAGE_SRC_URL, ");
			queryStr.append(" 	(CONCAT('resources',IMAGES.URL_PATH,PHOTO.USER_PTR,'/r_',IMAGES.FILE_NAME)) AS RESIZE_IMAGE_SRC_URL, ");
			queryStr.append(" 	IMAGES.PHYSICS_PATH, ");
			queryStr.append(" 	IMAGES.FILE_NAME ");
			queryStr.append(" FROM USER_INFO USER ");
			queryStr.append(" LEFT JOIN USER_PHOTO_INFO PHOTO ON USER.PKEY=PHOTO.USER_PTR ");
			queryStr.append(" LEFT JOIN USER_PHOTO_LIST_INFO IMAGES ON PHOTO.PKEY=IMAGES.GROUP_BY_PTR ");

			queryStr.append(" WHERE (CHECK_STATUS='1')");	
			
			if(userName.length() > 0){
				queryStr.append( "	AND USER.USER_NAME = '" + userName + "' " );
			}
			
			if(imageFileName.length() > 0){
				queryStr.append( "	AND IMAGES.FILE_NAME = '" + imageFileName + "' " );
			}
						
			queryStr.append(" limit 20 ");
			
		    list = (List<FilterUserImage>) this.hibernateTemplate.execute(new HibernateCallback<List<FilterUserImage>>() {
		    	
		    	public List<FilterUserImage> doInHibernate(final Session session)throws HibernateException, SQLException {
		    		
		    		Query query = session.createSQLQuery(queryStr.toString()).addEntity(FilterUserImage.class);
		    		
		    		return query.list();
		    	}
		    });

		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return list;		
	}
	
}
