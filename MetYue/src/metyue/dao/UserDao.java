package metyue.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import metyue.model.UserBaseInfo;
import metyue.model.UserContactInfo;
import metyue.model.UserInfo;
import metyue.model.UserPhotoInfo;
import metyue.model.UserPhotoListInfo;
import metyue.model.UserPhotoListVO;
import metyue.model.UserPurposeInfo;
import metyue.utils.MD5Util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserDao {

	@Autowired
    private HibernateTemplate hibernateTemplate;
	
	////////////////////////////////////////////////////////////////////////////////
	
	//删除一张图片
	public UserPhotoListInfo delPhotoListInfo(long photoListPtr){
		
		UserPhotoListInfo item = hibernateTemplate.get(UserPhotoListInfo.class, photoListPtr);
		
		//简单获取图片
		UserPhotoListInfo resultItem = new UserPhotoListInfo();
		resultItem.setPkey(item.getPkey());
		resultItem.setUrlPath(item.getUrlPath());
		resultItem.setPhysicsPath(item.getPhysicsPath());
		resultItem.setFileName(item.getFileName());	
		
		//记录删除
		hibernateTemplate.delete(item);
		
		return resultItem;
	}
	
	//0 不公开; 1 公开; 弹性开关
	public void displayPhotoList(long userPtr, long photoListPtr, String photoGroupName){
		
		UserPhotoInfo userPhotoInfo = geUserPhotoInfoByName(userPtr, photoGroupName);
		
		UserPhotoListInfo item = hibernateTemplate.get(UserPhotoListInfo.class, photoListPtr);
		
		String evel = item.getIsDisplay();
		
		if( ("1".endsWith(evel)) || ("0".endsWith(evel)) ) {
			
			item.setIsDisplay("1".endsWith(evel) ? "0": "1");
			item.setUserPhotoInfo(userPhotoInfo);
			
			hibernateTemplate.saveOrUpdate(item);	
		}
		
	}
	
	//封面操作: 1 封面  0 非封面
	public void changePhotoCover(long userPtr, long photoListPtr, String photoGroupName){		
		
		UserPhotoInfo userPhotoInfo = geUserPhotoInfoByName(userPtr, photoGroupName);	
		
		Iterator<UserPhotoListInfo> pSet = userPhotoInfo.getUserPhotoListInfos().iterator();
		
		while(pSet.hasNext() ){
			
			UserPhotoListInfo item = (UserPhotoListInfo)(pSet.next());
			
			item.setUserPhotoInfo(userPhotoInfo);
			
			if( photoListPtr==item.getPkey() ){
				
				item.setIsCover( "1".equals(item.getIsCover()) ? "0" : "1" );				
			}else{
				
				item.setIsCover("0");
			}
			
			hibernateTemplate.saveOrUpdate(item);
			
		}
		
	}
	
	//保存一个图片组的批量图片
	public UserPhotoInfo saveUserPhotoInfo(Set<UserPhotoListInfo> lists, long userPtr, String imageGroupName){
		
		UserPhotoInfo userPhotoInfo = geUserPhotoInfoByName(userPtr, imageGroupName);
		
		userPhotoInfo.setUserPhotoListInfos(lists);
		
		hibernateTemplate.saveOrUpdate(userPhotoInfo);
		
		return userPhotoInfo;
	}
	
	//保存一个图片组的一张图片
	public UserPhotoListInfo saveUserPhotoListInfo( UserPhotoListInfo item, long userPtr,String imageGroupName){		
		
		UserPhotoInfo userPhotoInfo = geUserPhotoInfoByName(userPtr, imageGroupName);
		
		item.setUserPhotoInfo(userPhotoInfo);
		
		hibernateTemplate.saveOrUpdate(item);
		
		return item;
	}
		
	//获取用户的所有图片组和图片集
	public Set<UserPhotoInfo> geUserPhotoInfo(long userPtr){
		
		return hibernateTemplate.get( UserInfo.class,  userPtr).getUserPhotoInfos();
	}
	
	//获取用户的所有图片组和图片集: list
	@SuppressWarnings("unchecked")
	public List<UserPhotoListVO> getUserPhotoInfoVOList(long userPtr, long groupPtr, boolean isMyself){
		
		List<UserPhotoListVO> list = new ArrayList<UserPhotoListVO>();
		
		try {
			
		    String queryStr1 = " FROM UserPhotoListVO WHERE userPtr = ? "; 
		    String queryStr2 = " FROM UserPhotoListVO WHERE userPtr = ? and groupPtr = ? ";
		    
		    //如果不公开的图片，不参与公共查询，除了在个人的图片查询里
	    	if(!isMyself){
	    		queryStr1 += " and displayFlg=1 and checkStatus=1 Order By lastUpdate Desc";
	    		queryStr2 += " and displayFlg=1 and checkStatus=1 Order By lastUpdate Desc";
	    	}
	    	
		    if( groupPtr <= 0L ){
		    	
		    	list = (List<UserPhotoListVO>)hibernateTemplate.find(queryStr1, userPtr );
		    	
		    }else{
		    	
		    	list = (List<UserPhotoListVO>)hibernateTemplate.find(queryStr2, userPtr, groupPtr );
		    }    
		    		    
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return list;		
	}
	
	
	//获取用户的所有图片组和图片集: list
	@SuppressWarnings("unchecked")
	public List<UserPhotoListVO> getUserPhotoInfoVOList(long userPtr, String groupName, boolean isMyself){
		
		List<UserPhotoListVO> list = new ArrayList<UserPhotoListVO>();
		
		try {
			
		    String queryStr1 = " FROM UserPhotoListVO WHERE userPtr = ? ";
		    String queryStr2 = " FROM UserPhotoListVO WHERE userPtr = ? and groupName = ? "; 
		    
		    //如果不公开的图片，不参与公共查询，除了在个人的图片查询里
	    	if(!isMyself){
	    		queryStr1 += " and displayFlg=1 and checkStatus=1 Order By lastUpdate Desc";
	    		queryStr2 += " and displayFlg=1 and checkStatus=1 Order By lastUpdate Desc";
	    	}
		    
		    if( groupName.length() == 0 ){
		    	
		    	list = (List<UserPhotoListVO>)hibernateTemplate.find(queryStr1, userPtr );
		    	
		    }else{
		    	
		    	list = (List<UserPhotoListVO>)hibernateTemplate.find(queryStr2, userPtr, groupName );
		    }	    
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return list;		
	}
	
		
	//获取用户的图片组：按组名称
	@SuppressWarnings("unchecked")
	public UserPhotoInfo geUserPhotoInfoByName(long userPtr, String photoGroupName)
	{
		
		UserPhotoInfo userPhotoInfo = null;	
		
		try {
			
		    String queryString = " "
		    		+ " FROM UserPhotoInfo AAA "
		    		+ " Left Join fetch AAA.userPhotoListInfos "
		    		+ " WHERE AAA.userInfo.pkey = ? and AAA.name = ? "; 
    
		    List<UserPhotoInfo> lst = new ArrayList<UserPhotoInfo>();
		    
		    lst = (List<UserPhotoInfo>)hibernateTemplate.find(queryString, userPtr, photoGroupName);
		    
		    if(lst.size() > 0){
		    	
		    	userPhotoInfo = lst.get(0);
		    }
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return userPhotoInfo;
	}
	
	//获取用户的图片组：按组名称
	@SuppressWarnings("unchecked")
	public UserPhotoInfo geUserPhotoInfoByGroupPtr(long userPtr, long imageGroupPtr)
	{
		
		UserPhotoInfo userPhotoInfo = null;
		
		try {
			
		    String queryString = " FROM UserPhotoInfo AAA WHERE AAA.userInfo.pkey = ? and AAA.pkey = ? "; 
		    List<UserPhotoInfo> lst = new ArrayList<UserPhotoInfo>();		    
		    lst = (List<UserPhotoInfo>)hibernateTemplate.find(queryString, userPtr, imageGroupPtr);
		    
		    if(lst.size() > 0){
		    	
		    	userPhotoInfo = lst.get(0);
		    }
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return userPhotoInfo;
	}	
	
	////////////////////////////////////////////////////////////////////////////////
	
	//保存并且获取用户基本资料
	public UserBaseInfo saveUserBaseInfo(UserBaseInfo userBaseInfo){
		
		long pkey = userBaseInfo.getUserPtr();
		
		UserInfo userInfo  = getUserInfoByPtr(pkey);
		
		userBaseInfo.setUserInfo(userInfo);
		
		hibernateTemplate.saveOrUpdate(userBaseInfo);
		
		return userBaseInfo; 
	}
	
	public UserBaseInfo getUserBaseInfoByPtr(Long userPtr )
	{
		
		UserBaseInfo item = null;
		
		try {
			
			item = hibernateTemplate.get( UserBaseInfo.class,  userPtr);
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return item;
	}
	
	
	//保存并且获取用户联系资料
	public UserContactInfo saveUserContactInfo(UserContactInfo userContactInfo){
		
		long pkey = userContactInfo.getUserPtr();
		
		UserInfo userInfo  = getUserInfoByPtr(pkey);
		
		userContactInfo.setUserInfo(userInfo);
		
		hibernateTemplate.saveOrUpdate(userContactInfo);
		
		return userContactInfo; 
	}
	
	public UserContactInfo getUserContactInfoByPtr(Long userPtr )
	{
		
		UserContactInfo item = null;
		
		try {
			
			item = hibernateTemplate.get( UserContactInfo.class,  userPtr);
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return item;
	}
	
	//保存并且获取用户宣言资料
	public UserPurposeInfo saveUserPurposeInfo(UserPurposeInfo userPurposeInfo){
		
		long pkey = userPurposeInfo.getUserPtr();
		
		UserInfo userInfo  = getUserInfoByPtr(pkey);
		
		userPurposeInfo.setUserInfo(userInfo);
		
		hibernateTemplate.saveOrUpdate(userPurposeInfo);
		
		return userPurposeInfo; 
	}
	
	public UserPurposeInfo getUserPurposeInfoByPtr(Long userPtr )
	{
		
		UserPurposeInfo item = null;
		
		try {
			
			item = hibernateTemplate.get( UserPurposeInfo.class,  userPtr);
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return item;
	}
	
	////////////////////////////////////////////////////////////////////////////////
	//保存
	public UserInfo save(UserInfo item){
			
		hibernateTemplate.save(item);
		
		return item; 
	}
	
	public UserInfo update(UserInfo item){
		
		hibernateTemplate.update(item);
		
		return item; 
	}
	
	//按用户 Email + Password: 获取  UserInfo
	@SuppressWarnings("unchecked")
	public UserInfo getUsetInfo(String userEmail, String userPwd)
	{
		
		UserInfo item = null;
		
		try {
			
		    String queryString = " FROM UserInfo WHERE userEmail = ? AND userPwd=? "; 
		    List<UserInfo> lst = new ArrayList<UserInfo>();		    
		    lst = (List<UserInfo>)hibernateTemplate.find(queryString, userEmail, MD5Util.MD5(userPwd) );
		    
		    if(lst.size() > 0){
		    	
		    	item = lst.get(0);
		    }
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return item;
	}
	
	
	//按用户 PKEY 获取 UserLogin
	public UserInfo getUserInfoByPtr(Long pkey )
	{
		
		UserInfo item = null;
		
		try {
			
			item = hibernateTemplate.get( UserInfo.class,  pkey);
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return item;
	}
	
	//获取用户 pkey : 用户登录 Email
	@SuppressWarnings("unchecked")
	public long getUserInfoPtrByEmail(String userEmail)
	{
		
		long pkey = -1L;
		
		try {
			
		    String queryString = " FROM UserInfo WHERE userEmail = ? "; 
		    List<UserInfo> lst = new ArrayList<UserInfo>();		    
		    lst = (List<UserInfo>)hibernateTemplate.find(queryString, userEmail);
		    
		    if(lst.size() > 0){
		    	
		    	pkey = lst.get(0).getPkey();
		    }
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return pkey;
	}
	
	
	//获取用户 pkey : 用户登录 Email
	@SuppressWarnings("unchecked")
	public UserInfo getUserInfoByEmail(String userEmail)
	{
		
		UserInfo userInfo = null;
		
		try {
			
		    String queryString = " FROM UserInfo WHERE userEmail = ? "; 
		    List<UserInfo> lst = new ArrayList<UserInfo>();		    
		    lst = (List<UserInfo>)hibernateTemplate.find(queryString, userEmail);
		    
		    if(lst.size() > 0){
		    	
		    	userInfo = lst.get(0);
		    }
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return userInfo;
	}
	
	//判断用户是否存在: 用户登录 Email
	@SuppressWarnings("unchecked")
	public boolean checkUserExistByEmail(String userEmail)
	{
		
		boolean result = false;
		
		try {
			
		    String queryString = " Select count(*) FROM UserInfo WHERE userEmail = ?";
		    List<Long> count = new ArrayList<Long>();
		    count = hibernateTemplate.find(queryString, userEmail );
		    
		    if( (count.size() > 0) && (count.get(0).intValue() > 0) ){
		    	
				result = true;
			}
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return result;
	}
	
}
