package metyue.dao;

import metyue.model.UserOnlineStatus;
import metyue.model.UserOptLogo;
import metyue.model.UserRejectStatus;
import metyue.model.UserSessionInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ImprintDao {

	@Autowired
    private HibernateTemplate hibernateTemplate;
	
	public void saveUserOnlineStatus(UserOnlineStatus userOnlineStatus){		

		hibernateTemplate.saveOrUpdate(userOnlineStatus);
	}
	
	public void saveUserRejectStatus(UserRejectStatus userRejectStatus){
		
		hibernateTemplate.saveOrUpdate(userRejectStatus);
	}
	
	public void saveUserOptLogo(UserOptLogo userOptLogo){
		
		hibernateTemplate.save(userOptLogo);
	}	
	
	public UserOnlineStatus getUserOnlineStatus(long userPtr){
		
		UserOnlineStatus item = null;
		
		try {
			
			item = hibernateTemplate.get( UserOnlineStatus.class,  userPtr);
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return item;
	}
	
	public UserRejectStatus getUserRejectStatus(long userPtr){
		
		UserRejectStatus item = null;
		
		try {
			
			item = hibernateTemplate.get( UserRejectStatus.class,  userPtr);
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return item;
	}
	
	public UserSessionInfo getUserSessionInfo(long userPtr){
		
		UserSessionInfo item = null;
		
		try {
			
			item = hibernateTemplate.get( UserSessionInfo.class,  userPtr);
		    
		} catch (Exception e) {
			
			e.printStackTrace();
	   	}
		
		return item;
	}
	
}
