package metyue.model;
// Generated 2014-7-26 9:21:38 by Hibernate Tools 3.6.0


import java.util.Date;

public class UserContactInfo  implements java.io.Serializable {

	private static final long serialVersionUID = -3408874656758073501L;
	private long userPtr;
     private UserInfo userInfo;
     private String tencentNum;
     private String tencentWeixien;
     private String sinaWeibo;
     private String renrenUrl;
     private String tel;
     private String othContactBy;
     private Date lastUpdate;
     private Long lastUserPtr;

    public UserContactInfo() {
    }
	
    public UserContactInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }    
    
    public UserContactInfo(long userPtr, String tencentNum,
			String tencentWeixien, String sinaWeibo, String renrenUrl,
			String tel, String othContactBy, Date lastUpdate, Long lastUserPtr) {
		super();
		this.userPtr = userPtr;
		this.tencentNum = tencentNum;
		this.tencentWeixien = tencentWeixien;
		this.sinaWeibo = sinaWeibo;
		this.renrenUrl = renrenUrl;
		this.tel = tel;
		this.othContactBy = othContactBy;
		this.lastUpdate = lastUpdate;
		this.lastUserPtr = lastUserPtr;
	}


	public UserContactInfo(UserInfo userInfo, String tencentNum, String tencentWeixien, String sinaWeibo, String renrenUrl, String tel, String othContactBy, Date lastUpdate, Long lastUserPtr) {
       this.userInfo = userInfo;
       this.tencentNum = tencentNum;
       this.tencentWeixien = tencentWeixien;
       this.sinaWeibo = sinaWeibo;
       this.renrenUrl = renrenUrl;
       this.tel = tel;
       this.othContactBy = othContactBy;
       this.lastUpdate = lastUpdate;
       this.lastUserPtr = lastUserPtr;
    }
   
    public long getUserPtr() {
        return this.userPtr;
    }
    
    public void setUserPtr(long userPtr) {
        this.userPtr = userPtr;
    }
    public UserInfo getUserInfo() {
        return this.userInfo;
    }
    
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
    public String getTencentNum() {
        return this.tencentNum;
    }
    
    public void setTencentNum(String tencentNum) {
        this.tencentNum = tencentNum;
    }
    public String getTencentWeixien() {
        return this.tencentWeixien;
    }
    
    public void setTencentWeixien(String tencentWeixien) {
        this.tencentWeixien = tencentWeixien;
    }
    public String getSinaWeibo() {
        return this.sinaWeibo;
    }
    
    public void setSinaWeibo(String sinaWeibo) {
        this.sinaWeibo = sinaWeibo;
    }
    public String getRenrenUrl() {
        return this.renrenUrl;
    }
    
    public void setRenrenUrl(String renrenUrl) {
        this.renrenUrl = renrenUrl;
    }
    public String getTel() {
        return this.tel;
    }
    
    public void setTel(String tel) {
        this.tel = tel;
    }
    public String getOthContactBy() {
        return this.othContactBy;
    }
    
    public void setOthContactBy(String othContactBy) {
        this.othContactBy = othContactBy;
    }
    public Date getLastUpdate() {
        return this.lastUpdate;
    }
    
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    public Long getLastUserPtr() {
        return this.lastUserPtr;
    }
    
    public void setLastUserPtr(Long lastUserPtr) {
        this.lastUserPtr = lastUserPtr;
    }




}


