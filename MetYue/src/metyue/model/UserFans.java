package metyue.model;

import java.util.Date;

public class UserFans implements java.io.Serializable{

	private static final long serialVersionUID = 7946790272521308577L;
	private long pkey;
	private long userPtr;
	private long fansPtr;
    private Date lastUpdate;
    private long lastUserPtr;
    
	public UserFans() {
		super();
	}

	public UserFans(long pkey, long userPtr, long fansPtr, Date lastUpdate,
			long lastUserPtr) {
		super();
		this.pkey = pkey;
		this.userPtr = userPtr;
		this.fansPtr = fansPtr;
		this.lastUpdate = lastUpdate;
		this.lastUserPtr = lastUserPtr;
	}

	public long getPkey() {
		return pkey;
	}

	public void setPkey(long pkey) {
		this.pkey = pkey;
	}

	public long getUserPtr() {
		return userPtr;
	}

	public void setUserPtr(long userPtr) {
		this.userPtr = userPtr;
	}

	public long getFansPtr() {
		return fansPtr;
	}

	public void setFansPtr(long fansPtr) {
		this.fansPtr = fansPtr;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public long getLastUserPtr() {
		return lastUserPtr;
	}

	public void setLastUserPtr(long lastUserPtr) {
		this.lastUserPtr = lastUserPtr;
	}
    
    
	
}
