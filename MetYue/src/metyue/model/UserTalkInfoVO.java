package metyue.model;

import java.util.Date;

public class UserTalkInfoVO implements java.io.Serializable{

	private static final long serialVersionUID = 4408361669770569124L;
	
	private long pkey;
	private long sendUserPtr;
	private String sendUserName;
	private String sendUserImageUrl;
	private long receiveUserPtr;
	private String receiveUserName;
	private String receiveUserImageUrl;
	private String talkAbout;
	private String readFlg;
	private Date readUpdate;
	private String readCssFlag;
	private String selfCssFlag;
	private String sendDate;
    private Date lastUpdate;
    private long lastUserPtr;
    
	public UserTalkInfoVO() {
		super();
	}

	public UserTalkInfoVO(long pkey, long sendUserPtr, String sendUserName,
			String sendUserImageUrl, long receiveUserPtr,
			String receiveUserName, String receiveUserImageUrl,
			String talkAbout, String readFlg, Date readUpdate,
			String readCssFlag, String selfCssFlag, String sendDate,
			Date lastUpdate, long lastUserPtr) {
		super();
		this.pkey = pkey;
		this.sendUserPtr = sendUserPtr;
		this.sendUserName = sendUserName;
		this.sendUserImageUrl = sendUserImageUrl;
		this.receiveUserPtr = receiveUserPtr;
		this.receiveUserName = receiveUserName;
		this.receiveUserImageUrl = receiveUserImageUrl;
		this.talkAbout = talkAbout;
		this.readFlg = readFlg;
		this.readUpdate = readUpdate;
		this.readCssFlag = readCssFlag;
		this.selfCssFlag = selfCssFlag;
		this.sendDate = sendDate;
		this.lastUpdate = lastUpdate;
		this.lastUserPtr = lastUserPtr;
	}

	public long getPkey() {
		return pkey;
	}

	public void setPkey(long pkey) {
		this.pkey = pkey;
	}

	public long getSendUserPtr() {
		return sendUserPtr;
	}

	public void setSendUserPtr(long sendUserPtr) {
		this.sendUserPtr = sendUserPtr;
	}

	public String getSendUserName() {
		return sendUserName;
	}

	public void setSendUserName(String sendUserName) {
		this.sendUserName = sendUserName;
	}

	public String getSendUserImageUrl() {
		return sendUserImageUrl;
	}

	public void setSendUserImageUrl(String sendUserImageUrl) {
		this.sendUserImageUrl = sendUserImageUrl;
	}

	public long getReceiveUserPtr() {
		return receiveUserPtr;
	}

	public void setReceiveUserPtr(long receiveUserPtr) {
		this.receiveUserPtr = receiveUserPtr;
	}

	public String getReceiveUserName() {
		return receiveUserName;
	}

	public void setReceiveUserName(String receiveUserName) {
		this.receiveUserName = receiveUserName;
	}

	public String getReceiveUserImageUrl() {
		return receiveUserImageUrl;
	}

	public void setReceiveUserImageUrl(String receiveUserImageUrl) {
		this.receiveUserImageUrl = receiveUserImageUrl;
	}

	public String getTalkAbout() {
		return talkAbout;
	}

	public void setTalkAbout(String talkAbout) {
		this.talkAbout = talkAbout;
	}

	public String getReadFlg() {
		return readFlg;
	}

	public void setReadFlg(String readFlg) {
		this.readFlg = readFlg;
	}

	public Date getReadUpdate() {
		return readUpdate;
	}

	public void setReadUpdate(Date readUpdate) {
		this.readUpdate = readUpdate;
	}

	public String getReadCssFlag() {
		return readCssFlag;
	}

	public void setReadCssFlag(String readCssFlag) {
		this.readCssFlag = readCssFlag;
	}

	public String getSelfCssFlag() {
		return selfCssFlag;
	}

	public void setSelfCssFlag(String selfCssFlag) {
		this.selfCssFlag = selfCssFlag;
	}

	public String getSendDate() {
		return sendDate;
	}

	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public long getLastUserPtr() {
		return lastUserPtr;
	}

	public void setLastUserPtr(long lastUserPtr) {
		this.lastUserPtr = lastUserPtr;
	}
		
}
