package metyue.model;

import java.util.Date;

public class UserTalkInfo implements java.io.Serializable{

	private static final long serialVersionUID = 264086894150009389L;
	
	private long pkey;
	private long sendUserPtr;
	private long receiveUserPtr;
	private String talkAbout;
	private String readFlg;
	private Date readUpdate;
	private String appSource;
    private Date lastUpdate;
    private long lastUserPtr;
    
	public UserTalkInfo() {
		super();
	}

	public UserTalkInfo(long pkey, long sendUserPtr, long receiveUserPtr,
			String talkAbout, String readFlg, Date readUpdate,
			String appSource, Date lastUpdate, long lastUserPtr) {
		super();
		this.pkey = pkey;
		this.sendUserPtr = sendUserPtr;
		this.receiveUserPtr = receiveUserPtr;
		this.talkAbout = talkAbout;
		this.readFlg = readFlg;
		this.readUpdate = readUpdate;
		this.appSource = appSource;
		this.lastUpdate = lastUpdate;
		this.lastUserPtr = lastUserPtr;
	}

	public long getPkey() {
		return pkey;
	}

	public void setPkey(long pkey) {
		this.pkey = pkey;
	}

	public long getSendUserPtr() {
		return sendUserPtr;
	}

	public void setSendUserPtr(long sendUserPtr) {
		this.sendUserPtr = sendUserPtr;
	}

	public long getReceiveUserPtr() {
		return receiveUserPtr;
	}

	public void setReceiveUserPtr(long receiverUserPtr) {
		this.receiveUserPtr = receiverUserPtr;
	}

	public String getTalkAbout() {
		return talkAbout;
	}

	public void setTalkAbout(String talkAbout) {
		this.talkAbout = talkAbout;
	}

	public String getReadFlg() {
		return readFlg;
	}

	public void setReadFlg(String readFlg) {
		this.readFlg = readFlg;
	}

	public Date getReadUpdate() {
		return readUpdate;
	}

	public void setReadUpdate(Date readUpdate) {
		this.readUpdate = readUpdate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public long getLastUserPtr() {
		return lastUserPtr;
	}

	public void setLastUserPtr(long lastUserPtr) {
		this.lastUserPtr = lastUserPtr;
	}

	public String getAppSource() {
		return appSource;
	}

	public void setAppSource(String appSource) {
		this.appSource = appSource;
	}
    
    
}
