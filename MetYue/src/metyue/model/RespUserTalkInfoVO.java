package metyue.model;

import java.util.List;

public class RespUserTalkInfoVO implements java.io.Serializable{

	private static final long serialVersionUID = -7301869327167038098L;

	private long posBegin;
	
	private long posEnd;
	
	private long pageSie;
	
	private long count;
	
	private List<UserTalkInfoVO> userTalkInfoVOs;

	public RespUserTalkInfoVO(long posBegin, long posEnd, long pageSie,
			long count, List<UserTalkInfoVO> userTalkInfoVOs) {
		super();
		this.posBegin = posBegin;
		this.posEnd = posEnd;
		this.pageSie = pageSie;
		this.count = count;
		this.userTalkInfoVOs = userTalkInfoVOs;
	}

	public long getPosBegin() {
		return posBegin;
	}

	public void setPosBegin(long posBegin) {
		this.posBegin = posBegin;
	}

	public long getPosEnd() {
		return posEnd;
	}

	public void setPosEnd(long posEnd) {
		this.posEnd = posEnd;
	}

	public long getPageSie() {
		return pageSie;
	}

	public void setPageSie(long pageSie) {
		this.pageSie = pageSie;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public List<UserTalkInfoVO> getUserTalkInfoVOs() {
		return userTalkInfoVOs;
	}

	public void setUserTalkInfoVOs(List<UserTalkInfoVO> userTalkInfoVOs) {
		this.userTalkInfoVOs = userTalkInfoVOs;
	}
	
	
}
