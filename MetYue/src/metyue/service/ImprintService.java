package metyue.service;

import metyue.dao.ImprintDao;
import metyue.model.UserOnlineStatus;
import metyue.model.UserOptLogo;
import metyue.model.UserRejectStatus;
import metyue.model.UserSessionInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImprintService {

	@Autowired
	private ImprintDao imprintDao;
	
	//用户注册: 更新用户状态下 + 用户操作日志 + 用户权限
	public void saveNewUserImprint(UserOnlineStatus userOnlineStatus, UserOptLogo userOptLogo, UserRejectStatus userRejectStatus){
		
		imprintDao.saveUserOnlineStatus(userOnlineStatus);
		imprintDao.saveUserOptLogo(userOptLogo);
		imprintDao.saveUserRejectStatus(userRejectStatus);
		
	}
	
	//用户操作：用户状态下 + 用户操作日志
	public void saveUserImprint(UserOnlineStatus userOnlineStatus, UserOptLogo userOptLogo){
		
		imprintDao.saveUserOnlineStatus(userOnlineStatus);
		imprintDao.saveUserOptLogo(userOptLogo);
		
	}
	
	//用户操作日志
	public void saveUserOptLogo(UserOptLogo userOptLogo){
		
		imprintDao.saveUserOptLogo(userOptLogo);
	}
	
	//获取用户状态
	public UserOnlineStatus getUserOnlineStatus(long userPtr){
		
		return imprintDao.getUserOnlineStatus(userPtr);
	}
	
	//save 用户状态
	public void saveUserOnlineStatus(UserOnlineStatus userOnlineStatus){
		
		imprintDao.saveUserOnlineStatus(userOnlineStatus);
	}
	
	//获取用户session 资料
	public UserSessionInfo getUserSessionInfo(long userPtr){
		
		return imprintDao.getUserSessionInfo(userPtr);
	}
	
}
