package metyue.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import metyue.dao.UserDao;
import metyue.model.UserBaseInfo;
import metyue.model.UserContactInfo;
import metyue.model.UserInfo;
import metyue.model.UserPhotoInfo;
import metyue.model.UserPhotoListInfo;
import metyue.model.UserPhotoListVO;
import metyue.model.UserPurposeInfo;
import metyue.utils.WebUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

	@Autowired
	private UserDao userDao;
	
    ///////////////////////////////////////////////////////////////////////
	
	public UserPhotoListInfo delPhotoList(long photoListPtr){
	
		return userDao.delPhotoListInfo(photoListPtr);
	}
	
	//用户操作: 0 不公开; 1 公开; 2 被禁
	public void displayPhotoList(long userPtr, long photoListPtr, String photoGroupName){
		
		userDao.displayPhotoList(userPtr, photoListPtr, photoGroupName);
	}
	
	public void changePhotoCover(long userPtr, long photoListPtr, String photoGroupName){
		
		userDao.changePhotoCover(userPtr, photoListPtr, photoGroupName);
	}
	
	public UserPhotoInfo saveUserPhotoInfo(Set<UserPhotoListInfo> lists, long userPtr, String imageGroupName){
		
		return userDao.saveUserPhotoInfo(lists, userPtr, imageGroupName);
	}

	public UserPhotoListInfo saveUserPhotoListInfo( UserPhotoListInfo item, long userPtr, String imageGroupName){
		
		return userDao.saveUserPhotoListInfo(item, userPtr, imageGroupName);
	}
	
	public List<UserPhotoListVO> getUserPhotoInfoVOList(long userPtr, long groupPtr, boolean isMyself){
			
		return userDao.getUserPhotoInfoVOList(userPtr, groupPtr, isMyself);
	}
	
	public List<UserPhotoListVO> getUserPhotoInfoVOList(long userPtr, String groupName, boolean isMyself){
		
		return userDao.getUserPhotoInfoVOList(userPtr, groupName, isMyself);
	}
	
	public UserPhotoInfo geUserPhotoInfoByName(long userPtr, String imageGroupName)	{
		
		return userDao.geUserPhotoInfoByName(userPtr, imageGroupName);
	}
	
	///////////////////////////////////////////////////////////////////////
	public UserBaseInfo saveUserBaseInfo(UserBaseInfo userBaseInfo){

		return userDao.saveUserBaseInfo(userBaseInfo);
	}
	
	public UserBaseInfo getUserBaseInfoByPtr(Long userPtr )
	{
		return userDao.getUserBaseInfoByPtr(userPtr);
	}	
	
	public UserContactInfo saveUserContactInfo(UserContactInfo userContactInfo){
		
		return userDao.saveUserContactInfo(userContactInfo);
	}
	
	public UserContactInfo getUserContactInfoByPtr(Long userPtr )
	{
		
		return userDao.getUserContactInfoByPtr(userPtr);
	}
	
	public UserPurposeInfo saveUserPurposeInfo(UserPurposeInfo userPurposeInfo){
		
		return userDao.saveUserPurposeInfo(userPurposeInfo);
	}
	
	public UserPurposeInfo getUserPurposeInfoByPtr(Long userPtr )
	{
		return userDao.getUserPurposeInfoByPtr(userPtr);

	}
	
	/////////////////////////////////////////////////////////////////////////
	public UserInfo register(UserInfo userInfo){
		
		//BaseInfo
		UserBaseInfo userBaseInfo = new UserBaseInfo();
		if("1".equals(userInfo.getSex().toString())){
			
			userBaseInfo.setAge(WebUtil.USER_BASE_INFO_MAN_AGE);
			userBaseInfo.setHeight(WebUtil.USER_BASE_INFO_MAN_HEIGHT);
			userBaseInfo.setWeight(WebUtil.USER_BASE_INFO_MAN_WEIGHT);
			
		}else{
			
			userBaseInfo.setAge(WebUtil.USER_BASE_INFO_GIRL_AGE);
			userBaseInfo.setHeight(WebUtil.USER_BASE_INFO_GIRL_HEIGHT);
			userBaseInfo.setWeight(WebUtil.USER_BASE_INFO_GIRL_WEIGHT);
		}
		
		userBaseInfo.setMarry(WebUtil.USER_BASE_INFO_MARRY);
		userBaseInfo.setJob(WebUtil.USER_BASE_INFO_JOB);
		userBaseInfo.setEducation(WebUtil.USER_BASE_INFO_EDUCATION);
		userBaseInfo.setUserInfo(userInfo);
		
		
		//UserPurposeInfo
		UserPurposeInfo userPurposeInfo = new UserPurposeInfo();
		userPurposeInfo.setPlan(WebUtil.USER_PURPOSE_INFO_PLAN);
		userPurposeInfo.setExpress(WebUtil.USER_PURPOSE_INFO_EXPRESS);
		userPurposeInfo.setManner(WebUtil.USER_PURPOSE_INFO_MANNER);
		userPurposeInfo.setUserInfo(userInfo);
		
		//UserContactInfo
		UserContactInfo userContactInfo = new UserContactInfo();
		userContactInfo.setUserInfo(userInfo);
		
		//UserPhotoInfo
		UserPhotoInfo photo = new UserPhotoInfo();
		photo.setUserInfo(userInfo);
		photo.setName(WebUtil.USER_PHOTO_DEFAULT_NAME);
		
		//UserInfo
		Set<UserPhotoInfo> userPhotoInfos = new HashSet<UserPhotoInfo>(0);
		userPhotoInfos.add(photo);
		userInfo.setUserPhotoInfos(userPhotoInfos);
		userInfo.setUserBaseInfo(userBaseInfo);
		userInfo.setUserContactInfo(userContactInfo);
		userInfo.setUserPurposeInfo(userPurposeInfo);
		userInfo.setManageLevel("0"); //默认为非管理人员
		
		
		return userDao.save(userInfo);
	}
	
	public UserInfo update(UserInfo userInfo){
		
		return userDao.update(userInfo);
	}
	
	public UserInfo login(String userEmail, String userPwd){
		
		return userDao.getUsetInfo(userEmail, userPwd);
	}
	
	public long getUserInfoPtrByEmail(String userEmail){
		
		return userDao.getUserInfoPtrByEmail(userEmail);
	}
	
	public UserInfo getUserInfoByEmail(String userEmail){
		
		return userDao.getUserInfoByEmail(userEmail);
	}
	
	public UserInfo getUserInfoByPkey(long userPtr){
		
		return userDao.getUserInfoByPtr(userPtr);
	}
	
	public boolean checkUserExistByEmail(String userEmail){
		
		return userDao.checkUserExistByEmail(userEmail);
	}
	
}
