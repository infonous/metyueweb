
var initManageMenuDs = [
	{
		text: "登录",
		encoded: true
	},
	{
	    text: "返回魅约网",
	    encoded: true
	}
];

var loginManageMenuDs = [
	{
	    text: "内容管理",
	    items: [
	            { text: "敏感词过滤"},
	            { text: "图片过滤"}
	    ]
	},
	{
	    text: "返回魅约网",
	    encoded: true
	},
	{
	    text: "退出",
	    encoded: true
	}
];

function extendUrl01(){	
	
	return "&AppSource=" + appSource + "&etc=" + new Date().getTime();
}

function extendUrl02(){	
	
	return "?AppSource=" + appSource + "&etc=" + new Date().getTime();
}

function initFixPageHeight() { 
	
    var nRealPageHeight = $(window).height() - $("#headWideDiv").height();     
    if (parseInt(nRealPageHeight) > parseInt("420")) {    	
        $("#contentWideDiv").height(nRealPageHeight - 2);  
        
    } else {
    	
        $("#contentWideDiv").height(420);
        
    }	
}

function doneMenu_init(){
	
	optMenu = $("#menuUL").kendoMenu({ 
		
        dataSource: initManageMenuDs,        
        select: onMenuSelect
	});
}

function doneMenu_login(){
	
	optMenu = $("#menuUL").kendoMenu({
		
        dataSource: loginManageMenuDs,         
        select: onMenuSelect
	});
}

function onMenuSelect(e){
	
	var menuTitle = $(e.item).children(".k-link").text();
	
	if(menuTitle=="登录"){	
		
		showLoginWindows();		
	}else if(menuTitle=="退出"){	
		
		logout();		
	}else if(menuTitle=="敏感词过滤"){
		
		showFilterWindows();
	}else if(menuTitle=="图片过滤"){
		
		showFilterPhotoWindows();
	}else if(menuTitle=="返回魅约网"){
		
		openNewWindow("http://www.metyue.com");
	}	
}

//初始化页面：自动判断用户是否已经在线
function initPage(){
	
	$('#loading').show();
    $.ajax({ 
	  	url: "user/checkOnline.json" + extendUrl02(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){
  				
  				if(json.result=='ON_LINE'){  					
  					closeLoginWindows();
  					doneMenu_login();
  		            	  					
  				}else if(json.result=='OFF_LINE'){  					
  					doneMenu_init();
  					showLoginWindows();
  					
  				}else{  					
  					doneMenu_init();
  					showLoginWindows();
  					
  				} 				
  			}
	  	}, 
	  	error: function(x, e) {
	  		
	  			doneMenu_init();	  			
	  			showLoginWindows();
        },
	  	complete: function(x) {
	  		$('#loading').hide();
	  	} 
	});
}

//用户退出
function logout(){
	
	$('#loading').show();
    $.ajax({ 
	  	url: "user/logout.json" + extendUrl02(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){  				
  				if( (json.result=='AUTO_OFF_LINE') || (json.result=='SUCCESS') ){  					
  					doneMenu_init();
  					closeAllWindows();
  					showLoginWindows();  					
  				} else{  					
  					showMsgBoxWindows(json.context);  					
  				}				
  			}
	  	}, 
	  	error: function(x, e) {	  		
	  		showMsgBoxWindows(errorMsg);	  		
        },
	  	complete: function(x) {
	  		$('#loading').hide();	  		
	  	} 
	});
	
}

//提示窗口
function createMsgboxWindows(){   
	
    if(optMsgBoxWindows == null){        
    	optMsgBoxWindows = $("#msgBoxWindowsDiv").kendoWindow({                
            width: "320px",
            height: "120px",
            title: "提示",
            resizable: false
        });      
    	
        $("#btnConfrim").kendoButton({        	
            click: function onClick(e) {  
            	
            	closeMsgBoxWindows();
            }
        });
    }
}

function clearMsgBoxFrom(){	
	
    $("#msgBoxValidator").text("");
}

function addMsgBoxFrom(tips){	
	
    $("#msgBoxValidator").text(tips);
}

function showMsgBoxWindows(tips){ 
	
    if(optMsgBoxWindows == null){        
    	createMsgboxWindows();
    	clearMsgBoxFrom();
    }   
    
    addMsgBoxFrom(tips);    
    optMsgBoxWindows.data("kendoWindow").open();
}

function closeMsgBoxWindows(){ 	
	
    if(optMsgBoxWindows != null){      
    	
    	optMsgBoxWindows.data("kendoWindow").close();
    }    
}



//用户登录
function makeLoginUrl(){	
    var email = $("#txtLoginEmail").val();
    var pwd = $("#txtLoginPwd").val();
    
	return "?Email=" + email + "&Pwd=" + pwd + extendUrl01();
}

function loginAjax(){
    
	$('#loginLoading').show();
    $.ajax({ 
	  	url: "user/login.json" + makeLoginUrl(), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){
	  		
  			if((json != null)&&(json!= "undefined")){  				
  				if(json.result=='SUCCESS'){
  					
  					doneMenu_login();
  					closeAllWindows();  
  					
  				}else{  					
                    $("#loginValidator").text(json.context)
                        .removeClass("valid")
                    	.addClass("invalid");                    
  				} 				
  			}
	  	}, 
	  	error: function(x, e) {	  		
				doneMenu_init(); 
        },
	  	complete: function(x) {	  		
	  		$('#loginLoading').hide();	  		
	  	} 
	});
}

function createLoginWindows(){
    
    if(optLoginWindows == null){
        
    	optLoginWindows = $("#loginWindowsDiv").kendoWindow({                
            width: "600px",
            height: "380px",
            title: "用户登录",
            resizable: false
        });
        
        $("#btnLogin").kendoButton({        	
            click: function onClick(e) {                 
                var loginName = $("#txtLoginEmail").val();
                var loginPwd = $("#txtLoginPwd").val();
                
                if((loginName.length == 0) || (loginPwd.length == 0)){                      
                    $("#loginValidator").text("请输入帐号和密码")
                    	.removeClass("valid")
                    	.addClass("invalid");                    
                }else{
                	
                	loginAjax();                    
                }
                
            }
        });
    }
}

function clearLoginFrom(){    
	
    $("#txtLoginEmail").val("");    
    $("#txtLoginPwd").val("");    
    $("#loginValidator").text("")    
        .removeClass("invalid")
        .removeClass("valid");
}

function showLoginWindows(){  
	
    if(optLoginWindows == null){        
        createLoginWindows();
    }    
    clearLoginFrom();    
    optLoginWindows.data("kendoWindow").open();
}

function closeLoginWindows(){ 
	
    if(optLoginWindows != null){        
    	optLoginWindows.data("kendoWindow").close();
    }    
}


//敏感词查询窗口
function createFilterWindows(){   
	
    if(optFilterWindows == null){        
    	optFilterWindows = $("#filterWindowsDiv").kendoWindow({                
            width: "600px",
            height: "420px",
            title: "敏感词查询窗口",
            resizable: false
        });   
    	
        $("#btnQueryFilter").kendoButton({        	
            click: function onClick(e) {
            	
            	var keyWords = $("#txtContentFilter").val();
            	
            	if(keyWords.length > 0 ){
            		
            		bindFilterView();
            		
            	}else{
            		
            		showMsgBoxWindows('关键词不能为空');
            	}          	
            }
        });
    }
}

function bindFilterView(){
    
	var keyWords = $("#txtContentFilter").val();
	
	if(keyWords.length == 0 ) return false;
	
	var filtercontentDS = new kendo.data.DataSource({
        transport: {
            read: {
                url: "manage/getFilterLists.json" + extendUrl02() + "&KeyWords=" + encode2(keyWords), 
                dataType: "json"
            }
        }
    });
    
    $("#filterView").kendoListView({
        dataSource: filtercontentDS,
        template: kendo.template($("#filterTemplate").html())
    });
     
}

function showFilterWindows(){ 	
	
    if(optFilterWindows == null){      
    	
    	createFilterWindows();
    }
    
    optFilterWindows.data("kendoWindow").open();
    
    //bindFilterView();
}

function closeFilterWindows(){ 	
	
    if(optFilterWindows != null){      
    	
    	optFilterWindows.data("kendoWindow").close();
    }    
}


//内容修改窗口
function doneFilerIt(pkey){
	
	var userName = $("#" + pkey + "_UserName").text();
	var purpose = $("#" + pkey + "_Purpose").text();
	
	$("#txtFilterContentUserName").val(userName);
	
	$("#txtFilterContentExplain").val(purpose);
	
	filterContentUserPtr = pkey;
		
	showFilterContentWindows();
}

function createFilterContentWindows(){   
	
    if(optFilterContentWindows == null){        
    	optFilterContentWindows = $("#filterContentWindowsDiv").kendoWindow({                
            width: "600px",
            height: "420px",
            title: "敏感内容修改窗口",
            resizable: false
        });   
    	
        $("#btnFilterContentUserName").kendoButton({        	
            click: function onClick(e) {
            	
            	var userName = $("#txtFilterContentUserName").val();
            	
            	if(userName.length > 0 ){
            		
            		filterContentUserNameAjax();
            		
            	}else{
            		
            		showMsgBoxWindows('帐号名称不能为空');
            	}          	
            }
        });
        
        $("#btnFilterContentExplain").kendoButton({        	
            click: function onClick(e) {
            	
            	filterContentPurposeAjax();          	
            }
        });
    }
}

function showFilterContentWindows(){ 	
	
    if(optFilterContentWindows == null){      
    	
    	createFilterContentWindows();
    }
    
    optFilterContentWindows.data("kendoWindow").open();
    
}

function closeFilterContentWindows(){ 	
	
    if(optFilterContentWindows != null){      
    	
    	optFilterContentWindows.data("kendoWindow").close();
    }    
}


//过滤帐号名称 
function filterContentUserNameAjax(){
    
	var userName = $("#txtFilterContentUserName").val();
	
	$('#filterContentLoading').show();
	
    $.ajax({ 
	  	url: "manage/filterUserName.json" + extendUrl02() + "&UserPtr=" + filterContentUserPtr + "&UserName=" + encode2(userName), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){
	  		
  			if((json != null)&&(json!= "undefined")){  				
  				if(json.result=='SUCCESS'){
                    $("#filterContentValidator").text(json.context)
	                    .removeClass("invalid")
	                	.addClass("valid"); 
  					
  				}else{  					
                    $("#filterContentValidator").text(json.context)
                        .removeClass("valid")
                    	.addClass("invalid");                    
  				} 				
  			}
	  	},
	  	complete: function(x) {	  		
	  		$('#filterContentLoading').hide();	  		
	  	} 
	});
}

//过滤帐号名称 
function filterContentPurposeAjax(){
    
	var purpose = $("#txtFilterContentExplain").val();
	
	$('#filterContentLoading').show();
	
    $.ajax({ 
	  	url: "manage/filterPurpose.json" + extendUrl02() + "&UserPtr=" + filterContentUserPtr + "&PurposeDesc=" + encode2(purpose), 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){
	  		
  			if((json != null)&&(json!= "undefined")){  				
  				if(json.result=='SUCCESS'){
                    $("#filterContentValidator").text(json.context)
	                    .removeClass("invalid")
	                	.addClass("valid"); 
  					
  				}else{  					
                    $("#filterContentValidator").text(json.context)
                        .removeClass("valid")
                    	.addClass("invalid");                    
  				} 				
  			}
	  	},
	  	complete: function(x) {	  		
	  		$('#filterContentLoading').hide();	  		
	  	} 
	});
}


//图片过滤处理窗口
function createFilterPhotoWindows(){
	
    if(optFilterPhotoWindows == null){        
    	optFilterPhotoWindows = $("#filterPhotoWindowsDiv").kendoWindow({                
            width: "600px",
            height: "480px",
            title: "图片过滤处理窗口",
            resizable: false
        });
    	
        $("#btnPhotoQueryFilter").kendoButton({        	
            click: function onClick(e) {            	

            	var userName = $("#textfilterPhotoQueryUserName").val();
            	var fileName = $("#textfilterPhotoQueryFileName").val();
            	
            	if((userName.length > 0 )  || (fileName.length > 0 )){
            		
            		bindFilterPhotoView();
            		
            	}else{
            		
                    $("#filterPhotoListValidator").text("帐号名称和文件名至少有一个不能为空")
	                    .removeClass("valid")
	                	.addClass("invalid");  
            		
            	}          	
            }
        });
    	
    }
}

function showFilterPhotoWindows(){
	
	if(optFilterPhotoWindows == null){  
		
		createFilterPhotoWindows();
	}
	
	optFilterPhotoWindows.data("kendoWindow").open();    
}

function closeFilterPhotoWindows(){ 	
	
    if(optFilterPhotoWindows != null){      
    	
    	optFilterPhotoWindows.data("kendoWindow").close();
    }    
}

function bindFilterPhotoView(){
	
	var userName = $("#textfilterPhotoQueryUserName").val();
	var fileName = $("#textfilterPhotoQueryFileName").val();
	
	if((userName.length == 0 ) && (fileName.length == 0 )){
		
		return false;		
	}
	
	var ds = new kendo.data.DataSource({
        transport: {
            read: {
                url: "manage/getFilterImageLists.json" + extendUrl02() + "&UserName=" + encode2(userName)+ "&ImageFileName=" + encode2(fileName), 
                dataType: "json"
            }
        }
    });
    
    $("#filterPhotoListView").kendoListView({
        dataSource: ds,
        template: kendo.template($("#filterPhotoTemplate").html())
    });
}

function t4_td_del_comte(self){
	
	$(self).parent().parent().html('');
}

function t4_td_del(self, userPtr, imagePtr){	
	
	$('#filterPhotoLoading').show();
    $.ajax({ 
	  	url: "manage/checkUploadImage.json" + extendUrl02() + "&PhotoListPtr=" + imagePtr + "&UserPtr=" + userPtr, 
	  	type: "POST", 
	  	dataType: "json",
	  	contentType: "application/json; charset=utf-8", 
	  	data: "{}",
	  	global: true,
	  	success: function(json){	  		
  			if((json != null)&&(json!= "undefined")){
  				
  				if(json.result=='SUCCESS'){  
  					
  					t4_td_del_comte(self);

                    $("#filterPhotoListValidator").text(json.context)
	                    .removeClass("invalid")
	                	.addClass("valid"); 
                    
  				}else{  					
                    $("#filterPhotoListValidator").text(json.context)
	                    .removeClass("valid")
	                	.addClass("invalid"); 
  				} 				
  			}
	  	}, 
	  	error: function(x, e) {
            $("#filterPhotoListValidator").text("操作失败")
	            .removeClass("valid")
	        	.addClass("invalid"); 
        },
	  	complete: function(x) {
	  		$('#filterPhotoLoading').hide();
	  	} 
	});	
	
}


//关闭全部窗口
function closeAllWindows(){

	closeMsgBoxWindows();
	closeLoginWindows();
	closeFilterWindows();
	closeFilterContentWindows();
	closeFilterPhotoWindows();
}





