<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Upload Photo</title>
    <link href="resources/script/kendoUI/styles/kendo.common.min.css" rel="stylesheet" type="text/css" />
    <link href="resources/script/kendoUI/styles/kendo.default.min.css" rel="stylesheet" type="text/css" /> 
    <style type="text/css">
		body 
		{
		    background: none repeat scroll 0 0 #FFFFFF; 
		    font: 12px/150% Arial,Verdana,"宋体","微软雅黑";
		    color: #3C3C3C;
		}
		
		* 
		{
		   margin: 0;
		   padding: 0;
		}
		
		div,form,img,ul,ol,li,dl,dt,dd,p
		{
		    margin: 0; 
		    padding: 0; 
		    border: 0; 
		}
		
		img 
		{
		    display: block; 
		    margin: 0px;
		    padding: 0px;
		} 
		
		li,dl
		{
		    list-style-type:none;
		}
				
		#uploadMsg
		{
			margin: 0 auto;
		    padding: 4px;
			color: red;
		}
		
		.lab{color: green;}
    </style>
        
    <script src="resources/script/kendoUI/js/jquery.min.js" type="text/javascript"></script>
    <script src="resources/script/kendoUI/js/kendo.web.min.js" type="text/javascript"></script>
</head>
<body>
	
	<%
		String AppSource = request.getParameter("AppSource") == null ? "" : request.getParameter("AppSource");
	%>
    
	<div class="k-content">            
	    <form method="post" action="upload/UploadUserPhoto.json?Category=<%=AppSource%>" method="post" 
	    		enctype="multipart/form-data" encoding="multipart/form-data"
	    		data-role="upload" multiple="multiple" autocomplete="off">
	        <div style=" margin: 0 auto; height:auto;">                
	            <input name="UserImagesFile" id="UserImagesFile" type="file" />
	            <div id="uploadMsg">
	            	<label class='lab'>提示：</label>
	            	<ul id="msgUl"><li>上传的图片格式为：jpg、gif、png，文件大小不要超过 120K</li></ul>
	            </div>
	        </div>
	    </form>
	    
	    <script type="text/javascript">
	        $(document).ready(function() {
	            $("#UserImagesFile").kendoUpload({
	            	showFileList: true,
	            	multiple: false,
	            	async: {
	                    saveUrl: "user/UploadUserPhoto.json?AppSource=<%=AppSource%>",
	                    autoUpload: true
	                },
	                upload: function onUpload(e) {
	                    var files = e.files;
	                    $.each(files, function() {
	                    	var msg = "";
	                        if (!((this.extension == ".jpg") || (this.extension == ".jpeg") || (this.extension == ".gif") || (this.extension == ".png"))) {	                            	                            
	                        	msg = "<li>请选择 jpg、gif、png 格式的图片文件;</li>";
	                        }
	                        
	                        if(this.size > 122880){	                            
	                        	msg = msg + "<li>图片文件的大小已经超过 120K;</li>"
	                        }
	                        
	                        if(msg.length > 0){
	                        	
	                        	$("#msgUl").html( msg );
	                        	e.preventDefault();
	                        }
	                        
	                    });
	                },
	                progress: function onProgress(e) {
	                    //var files = e.files;
	                },
	                success: function onSuccess(e) {
	                    var files = e.files;	                    
	                    if (e.operation == "upload") {	                        
	                        $("#msgUl").html( "<li>成功上传  " + files.length + " 个文件</li>" );
	                        
	                        //parent.bindUserPhotoInfo();
	                    }
	                }
	            });
	        });
	    </script>
	</div>


</body>
</html>